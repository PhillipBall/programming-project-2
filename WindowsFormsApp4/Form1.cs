﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem.ToString() == "Forest")
            {
                pictureBox1.Image = Properties.Resources.Forest1;
                pictureBox1.Visible = true;
            }
            if (listBox1.SelectedItem.ToString() == "Desert")
            {
                pictureBox1.Image = Properties.Resources.Desert1;
                pictureBox1.Visible = true;
            }
            if (listBox1.SelectedItem.ToString() == "Field")
            {
                pictureBox1.Image = Properties.Resources.Field1;
                pictureBox1.Visible = true;
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem.ToString() == "Field")
            {
                pictureBox1.Image = Properties.Resources.Field1;
                pictureBox1.Visible = true;
            }
            if (listBox1.SelectedItem.ToString() == "Desert")
            {
                pictureBox1.Image = Properties.Resources.Desert1;
                pictureBox1.Visible = true;
            }
            if (listBox1.SelectedItem.ToString() == "Forest")
            {
                pictureBox1.Image = Properties.Resources.Forest1;
                pictureBox1.Visible = true;
            }

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem.ToString() == "Forest")
            {
                pictureBox1.Image = Properties.Resources.Forest3;
                pictureBox1.Visible = true;
            }
            if (listBox1.SelectedItem.ToString() == "Desert")
            {
                pictureBox1.Image = Properties.Resources.Desert3;
                pictureBox1.Visible = true;
            }
            if (listBox1.SelectedItem.ToString() == "Field")
            {
                pictureBox1.Image = Properties.Resources.Field3;
                pictureBox1.Visible = true;
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                if (listBox1.SelectedItem.ToString() == "Field")
                {
                    pictureBox1.Image = Properties.Resources.Field2;
                    pictureBox1.Visible = true;
                }
                if (listBox1.SelectedItem.ToString() == "Desert")
                {
                    pictureBox1.Image = Properties.Resources.Desert_2;
                    pictureBox1.Visible = true;
                }
                if (listBox1.SelectedItem.ToString() == "Forest")
                {
                    pictureBox1.Image = Properties.Resources.Forest2;
                    pictureBox1.Visible = true;
                }
            }
            if(checkBox1.Checked == false)
            {
                if (listBox1.SelectedItem.ToString() == "Forest")
                {
                    pictureBox1.Image = Properties.Resources.Forest3;
                    pictureBox1.Visible = true;
                }
                if (listBox1.SelectedItem.ToString() == "Desert")
                {
                    pictureBox1.Image = Properties.Resources.Desert3;
                    pictureBox1.Visible = true;
                }
                if (listBox1.SelectedItem.ToString() == "Field")
                {
                    pictureBox1.Image = Properties.Resources.Field3;
                    pictureBox1.Visible = true;
                }

            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked == true)
            {
                if (listBox1.SelectedItem.ToString() == "Field")
                {
                    pictureBox1.Image = Properties.Resources.Field4;
                    pictureBox1.Visible = true;
                }
                if (listBox1.SelectedItem.ToString() == "Desert")
                {
                    pictureBox1.Image = Properties.Resources.Desert4;
                    pictureBox1.Visible = true;
                }
                if (listBox1.SelectedItem.ToString() == "Forest")
                {
                    pictureBox1.Image = Properties.Resources.Forest4;
                    pictureBox1.Visible = true;
                }
            }
            if (checkBox2.Checked == false)
            {
                if (listBox1.SelectedItem.ToString() == "Forest")
                {
                    pictureBox1.Image = Properties.Resources.Forest1;
                    pictureBox1.Visible = true;
                }
                if (listBox1.SelectedItem.ToString() == "Desert")
                {
                    pictureBox1.Image = Properties.Resources.Desert1;
                    pictureBox1.Visible = true;
                }
                if (listBox1.SelectedItem.ToString() == "Field")
                {
                    pictureBox1.Image = Properties.Resources.Field1;
                    pictureBox1.Visible = true;
                }
            }
        }
    }
}
